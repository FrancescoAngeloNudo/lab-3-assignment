public class Oven{
	
	public String brand;
	public double timer;
	public int heat;
	
	public void preheat(){
		System.out.println("Oven set to preheat untill " +heat+ " degrees");
	}
	
	public void timer(){
		System.out.println("Timer set for " +timer+ " minutes and seconds");
	}
	
}