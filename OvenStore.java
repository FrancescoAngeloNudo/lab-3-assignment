import java.util.Scanner;
public class OvenStore{
	
	public static void main (String[]args){
		Scanner sc = new Scanner(System.in);
		Oven[] ovens = new Oven[4];
		
		for(int i = 0; i < ovens.length; i++){
			ovens[i] = new Oven();
			System.out.println("Enter oven brand");
			ovens[i].brand = sc.nextLine();
			System.out.println("Set timer (--.-- format)");
			ovens[i].timer = sc.nextDouble();
			System.out.println("Set Temperature");
			ovens[i].heat = sc.nextInt();
			sc.nextLine();
		}
		
		System.out.println(ovens[3].brand);
		System.out.println(ovens[3].timer);
		System.out.println(ovens[3].heat);
		
		ovens[0].preheat();
		ovens[0].timer();
	}
}